CREATE DATABASE Hospital_Management;

Use  Hospital_Management;

CREATE TABLE Admin (
    a_id int primary key AUTO_INCREMENT,
    a_firstName varchar(20) ,
    a_lastName varchar(20),
    a_email varchar(30),
    a_password varchar(600),
    a_contact varchar(15),
    a_otp varchar(10)
);

insert into Admin values (default, "Rashmi", "Sinha", "rashmisinha2601@gmail.com","$2a$10$YFXMTVGGel6U5LMJvgUpBeH1O.jBn9hDu0r5CguZaeGG/B53Kq5MK","7004501389","Null");


insert into Admin values (default, "Salil", "Jhosi", "salilsj1619@gmail.com","$2a$10$d9RTKPbj.oP5nO/38/Bhy.fgFt7C4B3R1P4UnYNSv.09Yj3BKPrE6","8449055801","Null");


insert into Admin values (default, "Rakshit", "Sharma", "sharmarakshit27@gmail.com","$2a$10$YuqNNk0oJ8NFhPduY5hHjOypFjgv9K0vXRqv4i.4MkBrO/80V26ji","8192912648","Null");

insert into Admin values (default, "Prajakta", "Katkar", "prajaktakatkar1@gmail.com","$2a$10$udEm..8MuNzKJ.34miQuiuK5zZEw/J/u6iNxAo7dsS3CUIG5V6PkW","9689980078","Null");



CREATE TABLE Department (
    dept_id int primary key ,
    dept_name varchar(40)
);

insert into Department values (1, "Cardiology");

insert into Department values (2, "ENT");

insert into Department values (3, "Physician");

insert into Department values (4, "Neurology");

CREATE TABLE Doctor (
    d_id int primary key AUTO_INCREMENT,
    d_firstName varchar(20),
    d_lastName varchar(20),
    d_joinDate date,
    d_email varchar(30),
    d_password varchar(600),
    d_contact varchar(15),
    d_qualification varchar (200),
    d_status bit(1) ,
    d_address varchar(400),
    d_dob date,
    d_gender varchar(10),
    dept_id int,
    d_otp varchar(10),
    FOREIGN KEY (dept_id) REFERENCES Department(dept_id) 

);
INSERT into Doctor values (default, "Salil", "J", "2018-02-03","salil@gmail.com","$2a$10$HmKOP1jjea.v57ouJ3j9d.ULXRsP30B.DZH3LKDUp2MmdxWDlwoUm","9999999999","MBBS",1,"Dehradun",
"2000-05-04","Male",1,"NULL");
INSERT into Doctor values (default, "Rakshit", "S", "2019-02-03","rakshit@gmail.com","$2a$10$ixppozBPefhufrQvxye1qOFVggE/7gyGn1XjlN1bsZublG8KqFZ9K","9999999990","MBBS",1,"Dehradun",
"2001-05-01","Male",3,"NULL");


CREATE TABLE Patient (
    p_id int primary key AUTO_INCREMENT,
    p_firstName varchar(20),
    p_lastName varchar(20),
     p_email varchar(30),
    p_password varchar(600),
    p_contact varchar(15),
    p_gender varchar(10),
    p_dob date,
    p_bloodGroup varchar(10),
    p_address varchar(400),
    p_otp varchar(10)
   
 

);

Insert into patient values (default,"Nitu","Singh","nitu@gmail.com", "$2a$10$GadIx5QYa6uqgVN0s.WlbO61QG8jv2RHT7.OYEXkIIqAzIM17zCci","123456789", "Female", "2003-09-08", "B+ve", "Pune","NULL");
Insert into patient values (default,"Nitin","Singh","nitin@gmail.com", "$2a$10$S29KzX72oI49A5SAyidGoeCrmTog57Qa1xr1hG6U099UmUzb/uqPe","223456789", "Female", "2013-06-08", "O+ve", "Mumbai","NULL");

CREATE TABLE Appointment (
    ap_id int primary key AUTO_INCREMENT,
    p_id int,
       FOREIGN KEY (p_id) REFERENCES Patient(p_id),
     ap_date date,
     ap_time time,
      p_admitStatus bit(1),
      p_admitRecommendationStatus bit(1),
      p_treatment_status bit(1),
    p_ap_status bit(1), 
     ap_reason varchar(500),
        d_id int ,
   FOREIGN KEY (d_id) REFERENCES Doctor(d_id)  
     

);
Insert into appointment values (default, 1, "1991-06-03","12:03:04", 1, 1, 1, 1, "HeadAche", 1);
Insert into appointment values (default, 1, "1999-01-03","12:03:04", 1, 1, 1, 1, "Fever", 1);






/* changed line 82 , 83  */
CREATE TABLE Room (
    room_id int primary key ,
    room_type varchar(30),
    room_rate double,
    room_status bit(1),
    ap_id int,
    FOREIGN KEY (ap_id) REFERENCES Appointment(ap_id)
    
);

insert into Room values(101, "General", 5.0,1, 2);
insert into Room values(102, "AC", 1500,1, 1);

CREATE TABLE Bill (
    
    bill_id int primary key AUTO_INCREMENT,

    
    ap_id int,
 FOREIGN KEY (ap_id) REFERENCES Appointment(ap_id),

 discharge_date date,
  room_id int,
   FOREIGN KEY (room_id) REFERENCES Room(room_id)



);

insert into Bill values(default, 1,"2022-04-02", 101);




/* changed line 110 , 111  */
CREATE TABLE Prescription (
    prc_id int primary key AUTO_INCREMENT,
    prc_date date,
     ap_id int,
    FOREIGN KEY (ap_id) REFERENCES Appointment(ap_id),
     medicines varchar(6000)

);
insert into Prescription values(default, "2022-12-03",1,"Calpol");
insert into Prescription values(default, "2022-02-01",2,"Betadine");















